(ns stigmergy.widget
  (:require [reagent.core :as r]
            [garden.core :refer [css]]
            [goog.style]
            [stigmergy.widget.datagrid :as datagrid]))


(defonce contacts [{:person/first-name "Sonny"
                    :person/last-name  "Su"
                    :person/email      "sonny.su@foobar.com"
                    :person/telephone  "123"
                    :person/gender     "M"}
                   {:person/first-name "John"
                    :person/last-name  "Smith"
                    :person/email      "john.smith@foobar.com"
                    :person/telephone  "1234"
                    :person/gender     "M"}
                   {:person/first-name "Jane"
                    :person/last-name  "Doe"
                    :person/email      "jane.doe@foobar.com"
                    :person/telephone  "12345"
                    :person/gender     "F"}
                   {:person/first-name "Jane"
                    :person/last-name  "Austen"
                    :person/email      "jane.austen@foobar.com"
                    :person/telephone  "123456"
                    :person/gender     "F"}])

(defonce grid-state (r/atom {}))



(defn init! []
  (reset! grid-state {:stigmergy.widget.datagrid/columns [{:stigmergy.widget.datagrid/column-kw :person/first-name
                                                           :stigmergy.widget.datagrid/width 200}
                                                          {:stigmergy.widget.datagrid/column-kw :person/last-name
                                                           :stigmergy.widget.datagrid/width  200}
                                                          {:stigmergy.widget.datagrid/column-kw :person/email
                                                           :stigmergy.widget.datagrid/width 200 :stigmergy.widget.datagrid/weight 1.0}
                                                          {:stigmergy.widget.datagrid/column-kw :person/telephone
                                                           :stigmergy.widget.datagrid/width 150}
                                                          {:stigmergy.widget.datagrid/column-kw :person/gender
                                                           :stigmergy.widget.datagrid/width 100}]
                      :stigmergy.widget.datagrid/rows contacts
                      :stigmergy.widget.datagrid/width 1000
                      :stigmergy.widget.datagrid/height 500})
  
  (let [app-div (js/document.getElementById  "app")
        div [:div {:style {:width "100%"
                           :height "100%"}}
             [datagrid/main grid-state]
             ]]
    

    
    (goog.style/installStyles (css [[:body {:background-color :white
                                            :height "100%"
                                            :overflow :hidden}]
                                    ["#app"  {:height "100%"}]]))
    (r/render div app-div)))
