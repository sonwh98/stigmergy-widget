(ns stigmergy.widget.datagrid
  (:require [clojure.spec.alpha :as s]
            [reagent.core :as r ]
            [reagent.ratom :refer-macros [reaction]]
            [com.kaicode.material_girl :as mdl]
            [stigmergy.dom :as dom]))

(s/def ::width pos?)
(s/def ::height pos?)
(s/def ::weight (s/and pos?
                       (fn [val]
                         (<= val 1))))

(s/def ::column (s/keys :req [(or ::width ::weight ) ::column-kw]
                        :opt [::header-fn ::content-fn]))
(s/def ::rows (s/coll-of map?))

(s/def ::columns (s/and vector?
                               (fn [columns]
                                 (let [valid-configs (map (fn [column]
                                                            (let [valid? (s/valid? ::column column)]
                                                              (when-not valid?
                                                                (s/explain ::column column))
                                                              valid?))
                                                          columns)]
                                   (every? true? valid-configs)))))

(s/def ::datagrid (s/and (s/keys :req [::columns ::rows ::width ::height]
                                 :opt [::context-menu ::selected-rows ::sort-column])
                         #(s/valid? ::columns (::columns %))
                         #(s/valid? ::rows (::rows %))))

(defonce cell-border-width 1)
(defonce cell-css {:display :table-cell
                   :padding 0
                   :margin 0
                   :border (str cell-border-width "px solid #d9d9d9")})
(defonce prevent-copy-css {:-webkit-user-select :none})
(defonce table-row-css {:display :table-row})
(defonce selected-row-css {:background-color "#e6ffff"})
(defn width-css [width] {:min-width width
                         :width width
                         :max-width width})

(defn get-col-width [grid-state column-kw]
  (let [columns (::columns @grid-state)
        column (->> columns
                    (filter (fn [column]
                              (= column-kw (::column-kw column))))
                    first)
        total-width (- (::width @grid-state) 0)
        fixed-widths (->> columns
                          (map (fn [column]
                                 (::width column)))
                          (reduce +))
        leftover-width (- total-width fixed-widths)
        
        width (::width column)
        weight (::weight column)
        width+extra (if weight
                      (+ width (* weight leftover-width))
                      width)]
    width+extra))

(defn- sort-indicator [col-kw sort-column]
  (let [[sort-col-kw order] (-> @sort-column seq first)]
    (when (= sort-col-kw col-kw)
      (cond
        (= order :ascending) [:i {:class "material-icons"} "arrow_drop_up"]
        (= order :descending) [:i {:class "material-icons"} "arrow_drop_down"]))))

(defn- column-header [grid-state column]
  (let [col-kw (::column-kw column)
        header-txt (name col-kw)
        width (reaction (get-col-width grid-state col-kw))
        sort-column (reaction (::sort-column @grid-state))
        toggle-sort-column #(let [[_ order] (-> @sort-column seq first)
                                  order (cond
                                          (= order :ascending) :descending
                                          (= order :descending) :ascending
                                          :else :ascending)]
                              (swap! grid-state (fn [grid-state]
                                                  (let [rows (::rows grid-state)
                                                        comparator (cond
                                                                     (= order :ascending) >
                                                                     (= order :descending) <
                                                                     :else >)
                                                        
                                                        sorted-rows (if comparator
                                                                      (vec (sort-by (fn [entity]
                                                                                      (-> entity col-kw
                                                                                          (or "") str
                                                                                          clojure.string/lower-case))
                                                                                    comparator
                                                                                    rows))
                                                                      rows)]
                                                    (-> grid-state
                                                        (assoc ::sort-column {col-kw order})
                                                        (assoc ::rows sorted-rows))))))]
    [:div {:class "mdl-button mdl-js-button mdl-button--raised"
           :style (merge cell-css (width-css @width))
           :on-click toggle-sort-column}
     header-txt
     [sort-indicator col-kw sort-column]]))

(defn- plus-button [grid-state]
  [:button {:class "mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
            :style cell-css
            :on-click #(let []
                         (swap! grid-state update-in [::rows] conj {}))}
   
   [:i {:class "material-icons"} "add"]])

(defn- column-headers [grid-state]
  [:div {:style (merge table-row-css
                       prevent-copy-css)}
   [plus-button grid-state]
   (let [columns (::columns @grid-state)]
     (doall (for [column columns]
              ^{:key (::column-kw column)} [column-header grid-state column])))])

(defn- number-button [grid-state index]
  (let [toggle-selected-rows #(swap! grid-state update-in [::selected-rows] (fn [selected-rows]
                                                                              (if (nil? selected-rows)
                                                                                (conj #{} index)
                                                                                (if (contains? selected-rows index)
                                                                                  (set (remove (fn [selected-row-index]
                                                                                                 (= index selected-row-index))
                                                                                               selected-rows))
                                                                                  (conj selected-rows index)))))
        delete-selected-rows #(let [selected-rows (::selected-rows @grid-state)
                                    undeleted-rows (->> (::rows @grid-state)
                                                        (keep-indexed (fn [index row]
                                                                        (when-not (contains? selected-rows index)
                                                                          row)))
                                                        vec)]
                                (swap! grid-state (fn [grid-state]
                                                    (-> grid-state
                                                        (dissoc ::selected-rows)
                                                        (assoc ::rows undeleted-rows)))))
        
        delete [:a {:on-mouse-out #(swap! grid-state dissoc ::context-menu)
                    :on-click delete-selected-rows} "Delete"]
        show-context-menu (fn [evt]
                            (let [datagrid-div (dom/select-ancestor (.-target evt) #(= (.-className %)
                                                                                   "datagrid"))
                                  rec (.. datagrid-div getBoundingClientRect)
                                  x (-> (.-clientX evt)
                                        (- (.-left rec))
                                        (+ 15))
                                  y (-> (.-clientY evt)
                                        (- (.-top rec))
                                        (+ 20))]
                              (swap! grid-state
                                     (fn [current-grid-state]
                                       (-> current-grid-state
                                           (assoc-in [::context-menu :content] delete)
                                           (assoc-in [::context-menu :x] x)
                                           (assoc-in [::context-menu :y] y)))))
                            (.. evt preventDefault))]
    [:div {:class "mdl-button mdl-js-button mdl-button--raised"
           :style (merge  cell-css prevent-copy-css)
           :on-click toggle-selected-rows
           :on-context-menu show-context-menu}
     (inc index)]))

(defn cell [grid-state row-index column]
  (let [col-kw (::column-kw column)
        width (reaction (get-col-width  grid-state col-kw))
        row (r/cursor grid-state [::rows row-index])
        col-val (reaction (col-kw @row))]
    [:div {:content-editable true
           :suppress-content-editable-warning true
           :style (merge cell-css (width-css @width))
           :on-blur (fn [evt]
                      (let [div     (. evt -target)
                            content (. div -textContent)]
                        (swap! row assoc col-kw content)
                        (prn "blur=" content)))}
     @col-val]))

(defn- create-row-divs [grid-state]
  (let [rows (::rows @grid-state)
        columns (::columns @grid-state)
        selected-rows (::selected-rows @grid-state)]
    (doall (map-indexed (fn [row-index row]
                          (let [style (if (contains? selected-rows row-index)
                                        (merge table-row-css selected-row-css)
                                        table-row-css)]
                            ^{:key row-index} [:div {:style style}
                                               [number-button grid-state row-index]
                                               (doall (for [column columns
                                                            :let [col-kw (::column-kw column)]]
                                                        ^{:key col-kw} [cell grid-state row-index column]))]))
                        rows))))

(defn context-menu [grid-state]
  (let [cm (::context-menu @grid-state)
        x (or (:x cm) 100)
        y (or (:y cm) 100)
        content (:content cm)]
    (when content
      [:div {:style {:position :absolute
                     :left x
                     :top y
                     :border :solid
                     :background-color :yellow}
             :on-blur #(js/console.log "focus=" %)}
       content])))

(defn main [grid-state]
  [:div {:class "datagrid"
         :style {:height "100%"
                 :background-color :white}}
   
   [column-headers grid-state]
   (create-row-divs grid-state)
   [context-menu grid-state]])

