(ns stigmergy.widget.resizable-datagrid
  (:require [reagent.core :as r]
            [com.kaicode.mercury :as m]
            [stigmergy.widget.datagrid :as datagrid]
            [stigmergy.dom :as dom]))

;;resizable-datagrid wraps stigmergy.widget.datagrid
;;and listens for resize events from wim window manager and resizes the datagrid

(defn main [grid-state]
  (let [resize-topic (atom nil)
        resize-channel (atom nil)]
    (r/create-class {:component-did-mount (fn [this-component] 
                                            (let [this-element (r/dom-node this-component)
                                                  window-div (dom/select-ancestor this-element #(= (.-className %)
                                                                                                   "wim-window"))
                                                  wim-id (-> window-div (aget  "dataset" "wimId") js/parseInt)
                                                  title-bar-height 40
                                                  border-widths (-> @grid-state :stigmergy.widget.datagrid/columns count (+ 2) (* 2))
                                                  plus-button-width 64
                                                  topic (keyword (str "wim/resize-" wim-id))
                                                  channel (m/on topic (fn [[_ width height]]
                                                                        (let [width (- width border-widths plus-button-width)
                                                                              height (- height
                                                                                        (* 2 title-bar-height))]
                                                                          (swap! grid-state assoc
                                                                                 :stigmergy.widget.datagrid/width width
                                                                                 :stigmergy.widget.datagrid/height height))))
                                                  width (or (:stigmergy.widget.datagrid/width @grid-state)
                                                            (- (.-offsetWidth this-element)
                                                               border-widths
                                                               plus-button-width))
                                                  height (or (:stigmergy.widget.datagrid/height @grid-state)
                                                             (- (.-offsetHeight this-element)
                                                                title-bar-height))
                                                  set-window-prop-msg [:stigmergy.wim/set-window-prop {:wim/id wim-id :wim/width width :wim/height height}]]
                                              (reset! resize-topic topic)
                                              (reset! resize-channel channel)
                                              (m/broadcast set-window-prop-msg)))
                     :component-will-unmount (fn [this-component]
                                               (prn "unsubscribe " @resize-topic)
                                               (m/unsubscribe resize-channel @resize-topic))
                     :reagent-render (fn [grid-state]
                                       [datagrid/main grid-state])})))
