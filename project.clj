(defproject stigmergy/widget "0.1.0-SNAPSHOT"
  :description "A window manager for react.js component written in ClojureScript"
  :url "https://github.com/sonwh98/wim"
  :license {:name "Apache License 2.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"  :scope "provided"]
                 [org.clojure/clojurescript "1.9.946" :scope "provided"]
                 [reagent "0.7.0"  :scope "provided"]
                 [garden "1.3.3"]
                 [com.kaicode/material-girl "0.1.0-SNAPSHOT"]
                 [stigmergy/dom "0.1.0-SNAPSHOT"]]
  :plugins [[lein-cljsbuild "1.1.7"]]

  :min-lein-version "2.7.1"

  :clean-targets ^{:protect false}
  [:target-path
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-to]]

  :source-paths ["src/cljs" "src"]
  :resource-paths ["resources" "target/cljsbuild"]

  :cljsbuild  {:builds {:prod {:source-paths ["src/cljs"  "env/prod/cljs"]
                               :compiler {:main demo.prod
                                          :output-to "target/cljsbuild/public/js/app.js"
                                          :output-dir       "target/cljsbuild/public/js"
                                          :source-map       "target/cljsbuild/public/js/app.js.map"
                                          :asset-path "/js"
                                          :optimizations :advanced
                                          :verbose true
                                          :pretty-print  true
                                          :parallel-build true}}
                        :dev {:source-paths ["src/cljs" "env/dev/cljs"]
                              :figwheel {:on-jsload "stigmergy.widget/init!"}
                              :compiler {:main demo.dev
                                         :asset-path "/js/dev"
                                         :output-to "target/cljsbuild/public/js/dev/app.js"
                                         :output-dir "target/cljsbuild/public/js/dev"
                                         :source-map true
                                         :optimizations :none
                                         :verbose false
                                         :pretty-print  true
                                         :parallel-build true}}}}
  :figwheel  {:http-server-root "public"
              ;;:nrepl-port 7002
              :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"
                                 "cider.nrepl/cider-middleware"
                                 "refactor-nrepl.middleware/wrap-refactor"]
              :css-dirs ["resources/public/css"]}

  :profiles {:dev {:repl-options {:init-ns demo.repl
                                  :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :dependencies [[binaryage/devtools "0.9.7"]
                                  [ring/ring-mock "0.3.1"]
                                  [ring/ring-devel "1.6.2"]
                                  [prone "1.1.4"]
                                  [figwheel-sidecar "0.5.14"]
                                  [org.clojure/tools.nrepl "0.2.13"]
                                  [com.cemerick/piggieback "0.2.2"]]
                   :resource-paths ["env/dev/resources"]
                   :source-paths ["env/dev/clj"]
                   :plugins [[lein-figwheel "0.5.14"]
                             [cider/cider-nrepl "0.16.0-SNAPSHOT"]
                             [org.clojure/tools.namespace "0.3.0-alpha2"
                              :exclusions [org.clojure/tools.reader]]
                             [refactor-nrepl "2.0.0-SNAPSHOT"
                              :exclusions [org.clojure/clojure]]]}})
