(ns ^:figwheel-no-load demo.dev
  (:require [stigmergy.widget :as widget]
            [devtools.core :as devtools]))

(devtools/install!)

(enable-console-print!)

(widget/init!)
