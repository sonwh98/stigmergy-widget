(ns ^:figwheel-no-load demo.prod
  (:require [stigmergy.widget :as widget]))

(set! *print-fn* (fn [& _]))
(widget/init!)
